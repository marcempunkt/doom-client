module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:@typescript-eslint/recommended",
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: "latest",
    sourceType: "module"
  },
  plugins: [
    "react",
    "@typescript-eslint"
  ],
  rules: {
    /* disabled */
    "no-mixed-spaces-and-tabs": 0,
    "@typescript-eslint/no-inferrable-types": 0,
    "@typescript-eslint/no-non-null-assertion": 0,
    "@typescript-eslint/no-explicit-any": 0,
    /* warnings */
    "prefer-const": "warn",
    "no-case-declarations": "warn",
    "no-empty": "warn",
    "@typescript-eslint/no-extra-semi": "warn",
    "@typescript-eslint/ban-ts-comment": "warn",
    "@typescript-eslint/no-empty-function": "warn",
    "react-hooks/exhaustive-deps": "warn",
    /* custom */
    "no-unused-vars": "off",
    "no-irregular-whitespace": ["error", { "skipRegExps": true }],
    "@typescript-eslint/no-unused-vars": ["warn", { "argsIgnorePattern": "^_" }],
  },
  settings: {
    react: {
      version: "detect"
    },
  },
}
