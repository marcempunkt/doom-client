import React, { useState, useContext, createContext } from "react";

import { BlogContextValue } from "../ts/types/contextvalue_types";
import { Blog } from "../ts/types/blog_types";

export const BlogContext = createContext<BlogContextValue | undefined>(undefined);

export const useBlogContext = () => {
  /**
   * Consume BlogContext with error handling
   * @public
   *
   * returns {Context}    SocketContext
   */
  const context = useContext(BlogContext);
  if (context === undefined) {
    throw Error( "BlogContext is undefined! Are you consuming BlogContext outside of the BlogContextProvider?" );
  }
  return context;
};

interface Props {
  children: JSX.Element;
}

const BlogContextProvider: React.FC<Props> = (props: Props) => {

  const [blogs, setBlogs] = useState<Array<Blog>>([]);

  const reset = () => {
    /**
     * reset blogcontext state
     * @public
     */
    return;
  }

  return (
    <BlogContext.Provider
      value={{
	/* ~State~ */
	blogs, setBlogs,
	/* ~Methods~ */
	reset,
      }}>
      {props.children}
    </BlogContext.Provider>
  );

};

export default BlogContextProvider;
