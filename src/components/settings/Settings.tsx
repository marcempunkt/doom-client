import React, { useEffect } from "react";
import "./Settings.scss";

import { useAppContext } from "../../contexts/AppContext";
import { useModalContext } from "../../contexts/ModalContext"; import { AppContextValue, ModalContextValue } from "../../ts/types/contextvalue_types";

import ProfileSettings from "./ProfileSettings";
import ClientSettings from "./ClientSettings";
import CloseButton from "../../ui/CloseButton";
import About from "./About";

const Settings: React.FC = () => {

  const appContext: AppContextValue = useAppContext();
  const modalContext: ModalContextValue = useModalContext();

  const handleClose = () => appContext.setShowSettings(false);

  useEffect(() => {
    // IMPORTANT: 
    // In case of using this with React make sure you don't accidentally use import { KeyboardEvent } from "react";.
    // This leads to the not assignable to type 'EventListener' exception. (Credit: stackoverflow manu) 
    const settingsKeyHandler = (e: KeyboardEvent) => {

      const isModalShown: boolean = [
	modalContext.showReportMessageModal,
	modalContext.showDeleteMessageModal,
	modalContext.showDeleteChatModal,
	modalContext.showChangeProfileImageModal,
	modalContext.showChangeUsernameModal,
	modalContext.showChangeEmailModal,
	modalContext.showChangePasswordModal,
	modalContext.showDeleteAccountModal,
	modalContext.showProfileModal,
	modalContext.showRemoveFriendModal,
	modalContext.showLogoutModal,
	modalContext.showBlockUserModal,
	modalContext.showUnblockUserModal
      ].includes(true);

      if ((e.key === "Escape") && isModalShown === false) return handleClose()
    };

    window.removeEventListener("keydown", settingsKeyHandler)
    window.addEventListener("keydown", settingsKeyHandler);

    return () => window.removeEventListener("keydown", settingsKeyHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalContext]);


  return(
    <div className="settings">
      <CloseButton className="settings__closebutton" onClick={ handleClose } position="right" />
      <div className="settings--scrollable">
	<h1 className="settings__heading">My Profile</h1>
        <ProfileSettings />
	<h1 className="settings__heading">Client</h1>
	<ClientSettings />
	<h1 className="settings__heading">Video & Audio</h1>
	<ClientSettings />
	<h1 className="settings__heading">About</h1>
	<About />
      </div>
    </div>
  );
}

export default Settings;
