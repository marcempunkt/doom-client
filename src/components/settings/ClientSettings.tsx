import React, { ChangeEvent } from "react";
import "./ClientSettings.scss";

import { useAppContext } from "../../contexts/AppContext";
import { AppContextValue } from "../../ts/types/contextvalue_types";
import { Theme } from "../../ts/types/config_types";

const ClientSettings: React.FC = () => {
  /**
   * contains all necessary settings for the client
   * @component
   */
  const appContext: AppContextValue = useAppContext();

  const handleSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
    switch (e.currentTarget.value) {
      case "light":
	return appContext.setTheme(Theme.LIGHT);
      case "dark":
	return appContext.setTheme(Theme.DARK);
      case "nord-light":
	return appContext.setTheme(Theme.NORD_LIGHT);
      case "nord-dark":
	return appContext.setTheme(Theme.NORD_DARK);
      default:
	return appContext.setTheme(Theme.LIGHT);
    }
  };

  return(
    <div className="clientsettings">
      <section className="clientsettings__section">
	<label className="clientsettings__section__theme">Theme: </label>
	<select className="clientsettings__section__themechanger" onChange={ handleSelectChange } value={ appContext.theme }>
	  <option value={ Theme.LIGHT }>{ Theme.LIGHT }</option>
	  <option value={ Theme.DARK }>{ Theme.DARK }</option>
	  <option value={ Theme.NORD_LIGHT }>{ Theme.NORD_LIGHT }</option>
	  <option value={ Theme.NORD_DARK }>{ Theme.NORD_DARK }</option>
	</select>
      </section>
    </div>
  );
};

export default ClientSettings;
