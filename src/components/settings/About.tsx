import React from "react";
import "./About.scss";
import BabbelnLogo from "../../assets/logo/logo.png";

import { useAppContext } from "../../contexts/AppContext";
import { AppContextValue } from "../../ts/types/contextvalue_types";

import SVG from "../../assets/svg/moon.svg";

const About: React.FC = () => {

  const appContext: AppContextValue = useAppContext(); 

  return(
    <div className="about">
      <img src={ BabbelnLogo } alt="logo" className="about__logo" />
      <section className="about__description">
	<section className="about__description__appname">Babbeln <div className="about__description__appname__version">0.0.1_alpha</div></section>
	<p className="about__description__vision">
	  Babbeln is a free and open source video chat platform,
	  with the primary goal of privacy, secruity and user freedom.
	</p>
	<p className="about__description__license">
	  This program comes with absolutely no warranty.
	  See the <a href="https://gitlab.com/marcempunkt/babbeln-client/-/blob/master/LICENSE" rel="noopener noreferrer" target="_blank">
	  GNU General Public License, version 3 or later
	  </a> for details.
	</p>
	<p className="about__description__website">
	  <a href="https://babbeln.app" rel="noopener noreferrer" target="_blank">https://babbeln.app</a>
	</p>
	<p className="about__description__author">by Marc Mäurer</p>
      </section>
    </div>
  );
};

export default About;
