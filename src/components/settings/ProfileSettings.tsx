import React from "react";
import "./ProfileSettings.scss";

import { ReactComponent as Image } from "../../assets/feathericons/image.svg";

import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { UserContextValue, ModalContextValue } from "../../ts/types/contextvalue_types";

const ProfileSettings: React.FC = () => {
  /**
   * Profile Section of the Settings Component
   * contains all necessary settings for the user
   * @component
   */
  const userContext: UserContextValue = useUserContext();
  const modalContext: ModalContextValue = useModalContext();

  const handleUsernameEdit = () => modalContext.setShowChangeUsernameModal(true);
  const handleEmailEdit = () => modalContext.setShowChangeEmailModal(true);
  const handlePasswordEdit = () => modalContext.setShowChangePasswordModal(true);
  const handleDeleteAccount = () => modalContext.setShowDeleteAccountModal(true);
  const handleProfileImg = () => modalContext.setShowChangeProfileImageModal(true);

  return(
    <div className="profilesettings">

      <header className="profilesettings__header">
	<div className="profilesettings__header__profilepic" onClick={ handleProfileImg } >
          <img
            className={ "profilesettings__header__profilepic__img" + userContext.handleStatusClasses(userContext.status) }
	    alt="profile"
	    src={ userContext.avatar } />
	  <div className="profilesettings__header__profilepic__changeimagebtn"><Image /></div>
	</div>
        <span className="profilesettings__header__username" >
	  { userContext.loggedInUsername }
	</span>
      </header>

      <section className="profilesettings__section">

        <div className="profilesettings__section__item">
          <header>Username</header>
          <section>
            <span>{ userContext.loggedInUsername }</span>
            <button className="btn" onClick={ handleUsernameEdit }>Edit</button>
          </section>
        </div>

        <div className="profilesettings__section__item">
          <header>Email</header>
          <section>
            <span>{ userContext.email }</span>
            <button className="btn" onClick={ handleEmailEdit }>Edit</button>
          </section>
        </div>

        <div className="profilesettings__section__item">
          <header>Password</header>
          <section>
            <span>********</span>
            <button className="btn" onClick={ handlePasswordEdit }>Edit</button>
          </section>
        </div>

        <div className="profilesettings__section__item">
          <header>Active?</header>
          <section>
            <span>{ (userContext.isActive) ? "activated" : "not activated" }</span>
            <button className="btn">Send E-mail</button>
          </section>
        </div>

      </section>

      <button className="btn--error margin-top" onClick={ handleDeleteAccount }>Delete my Account</button>

    </div>
  );
};

export default ProfileSettings;
