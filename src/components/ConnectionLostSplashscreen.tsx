import React from "react";
import "./ConnectionLostSplashscreen.scss";
import { ReactComponent as ServerDown } from "../assets/svg/server_down.svg";

const ConnectionLostSplashscreen: React.FC = () => {

  return(
    <div className="connectionlostsplashscreen">
      <ServerDown className="connectionlostsplashscreen__illustration" />
      { "Ups, no connection to the server..." }
    </div>
  );
};

export default ConnectionLostSplashscreen;
