import React, { useState, useEffect, useRef, MutableRefObject } from "react";
import logger from "../../../utils/logger";
import "./CallWindowControls.scss";

import { ReactComponent as Maximize } from "../../../assets/feathericons/maximize.svg";
import { ReactComponent as Minimize } from "../../../assets/feathericons/minimize.svg";
import { ReactComponent as Mic } from "../../../assets/feathericons/mic.svg";
import { ReactComponent as MicOff } from "../../../assets/feathericons/mic-off.svg";
import { ReactComponent as Video } from "../../../assets/feathericons/video.svg";
import { ReactComponent as VideoOff } from "../../../assets/feathericons/video-off.svg";
import { ReactComponent as Monitor } from "../../../assets/feathericons/monitor.svg";
import { ReactComponent as PhoneMissed } from "../../../assets/feathericons/phone-missed.svg";

import { useCallContext } from "../../../contexts/CallContext";
import { useNotificationContext } from "../../../contexts/NotificationContext";
import { useAppContext } from "../../../contexts/AppContext";
import { useSocketContext } from "../../../contexts/SocketContext";
import { useModalContext } from "../../../contexts/ModalContext";
import { CallContextValue,
	 NotificationContextValue,
	 AppContextValue,
	 SocketContextValue,
	 ModalContextValue } from "../../../ts/types/contextvalue_types";
import { NotificationType } from "../../../ts/types/notification_types";
import { SocketState } from "../../../ts/types/socket_types";
import { CallingType } from "../../../ts/types/call_types";

interface Props {
  callwindowRef: MutableRefObject<HTMLDivElement | null>;
  show: boolean;
}

const CallWindowControls: React.FC<Props> = (props: Props) => {
  /**
   * Component for CallWindow Controls like:
   * Microphone, Webcam, Screenshare, EndCall, Fullscreen Btns
   * also handles if the CallWindow Component should be fullscreen or not
   * @component
   */
  const callContext: CallContextValue = useCallContext();
  const notifContext: NotificationContextValue = useNotificationContext();
  const appContext: AppContextValue = useAppContext();
  const socketContext: SocketContextValue = useSocketContext();
  const modalContext: ModalContextValue = useModalContext();

  const [fullscreen, setFullscreen] = useState<boolean>(false);
  const [pressedCam, setPressedCam] = useState<boolean>(false);
  const [pressedScreenshare, setPressedScreenshare] = useState<boolean>(false);

  /** Toggle Pressed Cam on if the user clicked on start video call button. */
  useEffect(() => { callContext.setShowCam(callContext.callingType === CallingType.Video); }, [callContext.callingType]);

  /**
   * In React setTimoute will close all props/state
   * For example if you subscribe to an ID, and later want to unsubscribe, it would be a bug if ID could change over time
   * but in this case we want to have the current/latest/updated value of socketContext.state
   * to break out of the setTimeout loop in sendMessage/deleteGloballyMessage etc.
   * 
   * Source:
   * Cristian Salcescu
   * https://medium.com/programming-essentials/how-to-access-the-state-in-settimeout-inside-a-react-function-component-39a9f031c76f
   * 
   * https://github.com/facebook/react/issues/14010
   */
  const socketStateRef = useRef<SocketState>(socketContext.state);
  socketStateRef.current = socketContext.state;

  const toggleMic = () => {
    /**
     * Turn on or off mic and accordingly change the stream
     * which will automatically update the peer connetion's stream
     */
    if (!callContext.localStream) return;

    const micNextState: boolean = !callContext.mic;
    callContext.localStream.getAudioTracks().map((track: MediaStreamTrack) => track.enabled = micNextState);
    callContext.setMic(micNextState);
  };

  const toggleCam = async () => {
    /**
     * Toggle the webcam
     * If the webcam gets toggled on then create new stream with the old audio + the new video tracks
     * And start the renegotiation process
     * 
     * FIXME: getUserMedia creates a whole new playback on OS level and doesn't free it though it's quite (doesn't send any data)
     * only in chromium based browsers!!!
     * 
     * @private
     */
    if (!callContext.peerConnection) return logger.error.blue("CallWindowControls", "peerConnection is null.");
    if (!callContext.localStream) return logger.error.blue("CallWindowControls", "localStream is null.");

    setPressedCam(true);

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => toggleCam(), 500);
      return logger.error.blue("CallWindowControls", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    const showCamNextState: boolean = !callContext.showCam;
    let newLocalStream: MediaStream | undefined;

    /* camera gets turned on
     * copy old stream, create new stream combine both & append to peer connection */
    if (showCamNextState) {
      logger.log.blue("CallWindowControls", "Webcam turned on");
      const existingStream: MediaStream = callContext.localStream;
      const newStream: MediaStream | void = await navigator.mediaDevices.getUserMedia({
	video: true,
	audio: true,
      }).catch((err: unknown) => {
	/* Stop trying to create a video call */
	console.error(`Couldn't create mediastream with your webcam: ${err}`);
	notifContext.create(NotificationType.ERROR, "Failed to get webcam source.");
	setPressedCam(false);
      });

      if (!newStream) return;

      newLocalStream = new MediaStream([
	...newStream.getVideoTracks(),
	...existingStream.getAudioTracks(),
      ]);
    }

    /* camera gets turned off
     * A track.stop does not trigger any event (like: "track" or "ended")
     * to recreate the stream only with audio */
    if (!showCamNextState) {
      logger.log.blue("CallWindowControls", "Webcam turned off.");
      callContext.localStream.getVideoTracks().map((track: MediaStreamTrack) => track.stop());

      newLocalStream = await navigator.mediaDevices.getUserMedia({
	video: false,
	audio: true,
      });
    }

    callContext.setLocalStream(newLocalStream!);

    /* Remove old tracks from peer connection */
    const senders: Array<RTCRtpSender> = callContext.peerConnection.getSenders();
    senders.map((sender: RTCRtpSender) => callContext.peerConnection!.removeTrack(sender));
    /* apply mic disabled if necessary */
    if (!callContext.mic) {
      newLocalStream!.getAudioTracks().map((track: MediaStreamTrack) => track.enabled = false);
    }
    /* add the new media stream to the peer connection */
    newLocalStream!.getTracks().forEach((track: MediaStreamTrack) => {
      callContext.peerConnection!.addTrack(track, newLocalStream!);
    });
    /* Start renegotiation process with remote peer */
    await callContext.startRenegotiation();
    callContext.setShowCam(showCamNextState); 
    setPressedCam(false);
  };

  const toggleScreenshare = async () => {
    // return callContext.setShowScreen(!callContext.showScreen); // FIXME TODO
    /**
     */
    setPressedScreenshare(true);

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => toggleScreenshare(), 500);
      return logger.error.blue("CallWindowControls", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    const showScreenNextState: boolean = !callContext.showScreen;
    const isElectron: boolean = appContext.isElectron();

    if (showScreenNextState) {
      logger.log.blue("CallWindowControls", "Screenshare turned on.");

      if (isElectron) {
	/* show the screenshare modal picker for electron */
	modalContext.setShowScreenshareModal(true);
	setPressedScreenshare(false);
      }

      if (!isElectron) {
	/* use the browser api for creating a screenshare stream */
	const stream: MediaStream | undefined = await navigator.mediaDevices.getDisplayMedia({
	  video: true,
	  audio: false
	}).catch((err: unknown) => {
	  console.error(`${err}`);
	  return undefined;
	});

	/** The Browser will create a pop up menu to close/stop the screenshare
	 *  in that case the "ended" event will be triggered */
	if (stream) {
	  stream.getVideoTracks()[0].addEventListener("ended", (event: Event) => {
	    logger.log.blue("CallWindowControls", "localScreenshareStream ended.");
	    callContext.screenshareClose();
	    callContext.setShowScreen(false);
	  });
	}

	if (!stream) {
	  setPressedScreenshare(false);
	  return notifContext.create(NotificationType.ERROR, "Couldn't create stream.");
	}

	callContext.startScreenshareSignaling(stream);
      }
    } else {
      /* remove your own stream */
      logger.log.blue("CallWindowControls", "Screenshare turned off.")
      callContext.screenshareClose();
    }

    callContext.setShowScreen(!callContext.showScreen)
    setPressedScreenshare(false);
  };

  const toggleFullscreen = async () => {
    /**
     * Maximize or Minimize the Callwindow (fullscreen)
     */
    if (!props.callwindowRef?.current || !document) return;
    if (!document.fullscreenEnabled) return; /* fullscreen is not supported */

    /* FIXME Brave Browser handles exit fullscreen differently with <Escape>-Key
     * the state won't change unless the fullscreen btn gets clicked *small bug* */
    if (document.fullscreen) {
      await document.exitFullscreen();
      return setFullscreen(false);
    } else {
      props.callwindowRef.current.requestFullscreen();
      return setFullscreen(true);
    }
  };

  useEffect(() => {
    /**
     * If the user hits <Exit> it automatically exits all fullscreen elements of the site
     * If <Exit> => set the fullscreen state to false
     * IMPORTANT: 
     * In case of using this with React make sure you don't accidentally use import { KeyboardEvent } from "react";.
     * This leads to the not assignable to type 'EventListener' exception. (Credit: stackoverflow manu) 
     * @effect
     */
    const callWindowControlsKeyHandler = (e: KeyboardEvent) => {
      if (e.key === "Escape") {
	document.exitFullscreen();
	setFullscreen(false);
      }
    };
    window.addEventListener("keydown", callWindowControlsKeyHandler);
    return () => window.removeEventListener("keydown", callWindowControlsKeyHandler);
  }, []);

  return(
    <section className={ "callwindow__controls callwindow__controls" + ((props.show) ? "--show" : "--hide") }>

      { /* mute button */ }
      <button
	className={ "callwindow__controls__default" + (callContext.mic ? "--active" : "") }
	onClick={ toggleMic }
	title="toggle audio"
      >
	{ callContext.mic ? <Mic /> : <MicOff /> }
      </button>

      { /* toggle cam */ }
      <button
	className={ "callwindow__controls__default" + (callContext.showCam ? "--active" : "") }
	onClick={ toggleCam }
	title="toggle webcam"
	disabled={ pressedCam }
      >
	{ callContext.showCam ? <Video /> : <VideoOff /> }
      </button>

      { /* toggle screenshare */ }
      <button
	className={ "callwindow__controls__default" + (callContext.showScreen ? "--active" : "") }
	onClick={ toggleScreenshare }
	title="Screenshare"
	disabled={ pressedScreenshare }
      >
	<Monitor />
      </button>

      { /* end call */ }
      <button
	className="callwindow__controls__red"
	onClick={ () => callContext.hangup("emit") }
	title="Cancel call"
      >
	<PhoneMissed />
      </button>

      <button className="callwindow__controls__fullscreen" onClick={ toggleFullscreen }>
	{ (fullscreen) ? <Minimize /> : <Maximize /> }
      </button>
    </section>
  );
};

export default CallWindowControls;
