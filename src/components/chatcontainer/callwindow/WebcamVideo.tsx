import React, { useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import "./WebcamVideo.scss";

interface Props {
  stream: MediaStream | null;
  avatar: string;
}

const WebcamVideo: React.FC<Props> = (props: Props) => {

  const [id] = useState<string>("a" + uuidv4().split("-").join(""));
  const [showStream, setShowStream] = useState<boolean>(false);

  useEffect(() => {
    /**
     * Set the webcam video stream to the video html tag
     * with vanilla js because there are some issues with chrome
     * using react's useRef
     * @effect
     */
    const localVideo: HTMLMediaElement | null = document.querySelector("#" + id);

    if (localVideo && showStream) {
      localVideo.srcObject = props.stream;
    }

    return () => {
      if (!localVideo) return;
      localVideo.pause();
      localVideo.srcObject = null;
      localVideo.load();
    }
  }, [props.stream, id, showStream]);

  useEffect(() => {
    /**
     * Check if the stream that is passed as a prop
     * into this component has a video stream that
     * should be displayed
     * @effect
     */
    const isWebcamStream: boolean = (props.stream?.getVideoTracks().length) ? true : false;
    if (isWebcamStream) {
      setShowStream(true);
    } else {
      setShowStream(false);
    }

    return () => setShowStream(false);
  }, [props.stream]);

  return(
    (showStream) ?
    <video className="webcamvideo--video" id={ id } playsInline autoPlay muted ></video>
    :
    <img className="webcamvideo--img" alt={ "avatar" } src={ props.avatar }/>
  );
};

export default WebcamVideo;
