import React, { useState, useEffect } from "react";
import { ReactComponent as MosaicIcon } from "../../../assets/feathericons/grid.svg";
import { ReactComponent as LivestreamIcon } from "../../../assets/feathericons/aperture.svg";
import logger from "../../../utils/logger";
import "./LayoutPicker.scss";

import { useCallContext } from "../../../contexts/CallContext";
import { CallContextValue } from "../../../ts/types/contextvalue_types";
import { Layout } from "../../../ts/types/call_types";

interface Props {
  show: boolean;
}

const LayoutPicker: React.FC<Props> = (props: Props) => {

  const callContext: CallContextValue = useCallContext();

  const [showDropdown, setShowDropdown] = useState<boolean>(false);

  const handleMouseOut = () => setShowDropdown(false);

  const handleLayoutClick = () => setShowDropdown(!showDropdown);

  const handleChangeLayout = (layout: Layout) => {
    switch (layout) {
      case Layout.Mosaic: {
	setShowDropdown(false);
	return callContext.setCallWindowLayout(Layout.Mosaic);
      }
      case Layout.Livestream: {
	setShowDropdown(false);
	return callContext.setCallWindowLayout(Layout.Livestream);
      }
      default: {
	setShowDropdown(false);
	return logger.error.blue("LayoutPicker", `Unknown Layout value: ${layout}`);
      }
    }
  };

  const currentLayoutIcon: () => JSX.Element = () => {
    switch (callContext.callWindowLayout) {
      case Layout.Mosaic: {
	return <MosaicIcon />;
      }
      case Layout.Livestream: {
	return <LivestreamIcon />;
      }
      default: {
	return <MosaicIcon />;
      }
    }
  };

  useEffect(() => {
    /**
     * reset showdropdown when ui gets hidden
     * @effect
     */
    if (!props.show) { setShowDropdown(false); }
  }, [props.show]);

  return(
    <div className={ "layoutpicker" + ((props.show) ? "--show" : "--hide") } onMouseLeave={ handleMouseOut }>
      <div className="layoutpicker__view" onClick={ handleLayoutClick }>{ currentLayoutIcon() }Layout</div>
      <ul className={ "layoutpicker__list" + ((showDropdown) ? "--show" : "--hide") }>
	<li className="layoutpicker__list__item" onClick={ () => handleChangeLayout(Layout.Mosaic) }>
	  <MosaicIcon />
	  Mosaic
	</li>
	<li className="layoutpicker__list__item" onClick={ () => handleChangeLayout(Layout.Livestream) }>
	  <LivestreamIcon />
	  Livestream
	</li>
      </ul>
    </div>
  );
};

export default LayoutPicker;
