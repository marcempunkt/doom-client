import React from "react";
import "./TimelinePreview.scss";

import { useAppContext } from "../../contexts/AppContext";
import { AppContextValue } from "../../ts/types/contextvalue_types";

const TimelinePreview: React.FC = () => {

  const appContext: AppContextValue = useAppContext();

  const handleClick: () => void = () => {
    appContext.setShowDashboard(false);
    appContext.setShowTimeline(true);
  }

  return(
    <div className="timelinepreview" onClick={ handleClick }>
      Timeline preview
    </div>
  );
};

export default TimelinePreview;
