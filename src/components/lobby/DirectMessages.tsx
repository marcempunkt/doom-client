import React, { useState, useEffect } from "react";
import "./DirectMessages.scss";

import { useMessageContext } from "../../contexts/MessageContext";
import { useUserContext } from "../../contexts/UserContext";
import { useAppContext } from "../../contexts/AppContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { MessageContextValue, UserContextValue, AppContextValue, FriendsContextValue } from "../../ts/types/contextvalue_types";
import { ClientMessage } from "../../ts/types/message_types";

import DirectMessagesUserlist from "./DirectMessagesUserlist";

const DirectMessages: React.FC = () => {
    /**
     * Displays a list of Users, with which the currently logged in user has sent messages to
     * inside the Lobby Component
     * @component
     */
    const [directMsgs, setDirectMsgs] = useState<Array<number>>([]);

    const messageContext: MessageContextValue = useMessageContext();
    const userContext: UserContextValue = useUserContext();
    const appContext: AppContextValue = useAppContext();
    const friendsContext: FriendsContextValue = useFriendsContext();

    const getDirectMsgsUsers = () => {
        /**
         * Gets an Array of all Users you have a conversation with 
         * @private
         * 
         * returns  {Array<number>}  userIds
         */
        const directMsgsUser: Array<number> = [];

        messageContext.messages.map((msg: ClientMessage) => {
            const iSentTheMsg: boolean = msg.from_user === userContext.loggedInUserId;
            const friendSentTheMsg: boolean = msg.to_user === userContext.loggedInUserId;
            if (iSentTheMsg) {
                return directMsgsUser.push(msg.to_user);
            } else if (friendSentTheMsg) {
	        return directMsgsUser.push(msg.from_user);
            } else{
	        return null;
            } 
        });

        /* delete duplicate */
        const users: Array<number> = [...(new Set(directMsgsUser))];
        return users;
    };

    const getSortedDirectMsgsUsers: (userIds: Array<number>) => Array<number>  = (userIds) => {
        /**
         * Returns a chronologically sorted Array of userIds
         * @private
         * 
         * returns  {Array<number>}  userIds
         */

        let result: Array<{ userId: number; timestamp: number }> = [];

        /* Starting point for sorting all messages */
        let timestamp: number = 0;

        userIds.map((userId: number) => {
            /* Go through all SEND_MESSAGES */
            messageContext.messages.map((msg: ClientMessage) => {
                if ((msg.to_user === userId || msg.from_user === userId) &&  msg.send_at > timestamp) {
                    return timestamp = msg.send_at;
                } else {
	            return null; 
	        }
            });

            result.push({ userId , timestamp });

            /* Reset timestamp */
            return timestamp = 0;
        });

        /* Sort result chronologically */
        result.sort(function sort<T extends {
            userId: number;
            timestamp: number
        }>(y: T, x: T) {
            return x.timestamp - y.timestamp
        });

        /* Creat an Array that only contains the userIds without the timestamp */
        const sortedUsers: Array<number> = [];
        result.map((obj: { userId: number; timestamp: number }) => sortedUsers.push(obj.userId));

        return sortedUsers;
    };

    useEffect(() => {
        /**
         * Get a sorted Array of all userIds sorted chronologically after which you have/had conversations with
         * @effect
         */
        const users = getDirectMsgsUsers();
        const sortedUsers = getSortedDirectMsgsUsers(users);
        setDirectMsgs(sortedUsers);
        // console.log({ sortedUsers }); DEBUG
    }, [messageContext.messages, friendsContext.friends]);

    return(
        <div className="directmessages">
            <h1 className="directmessages__header">Direct Messages</h1>
            <DirectMessagesUserlist directMessages={ directMsgs } />
        </div>
    );
};

export default DirectMessages;
