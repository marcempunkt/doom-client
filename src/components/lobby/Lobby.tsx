import React from "react";
import "./Lobby.scss";

import ProfileSection from "./ProfileSection";
import DirectMessages from ".//DirectMessages";
import Quickmenu from "./Quickmenu";

const Lobby: React.FC = () => {

  return(
    <div className="lobby">
      <ProfileSection />
      <Quickmenu />
      <DirectMessages />
    </div>
  );
}

export default Lobby;
