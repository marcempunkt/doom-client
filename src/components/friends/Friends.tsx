import React from "react";
import "./Friends.scss";

import { ReactComponent as Users } from "../../assets/feathericons/users.svg";

import { FriendsContextValue } from "../../ts/types/contextvalue_types";
import { useFriendsContext } from "../../contexts/FriendsContext";

import AddFriend from "./AddFriend";
import Pendings from "./Pendings";
import BlockedFriends from "./BlockedFriends";
import AllFriends from "./AllFriends";

const Friends: React.FC = () => {
  /**
   * Big Block Component that contains everything in it that
   * has something to do with friends
   * @component
   */
  const friendsContext: FriendsContextValue = useFriendsContext();

  const setStateToFalse = () => {
    friendsContext.setShowOnline(false);
    friendsContext.setShowAll(false);
    friendsContext.setShowPending(false);
    friendsContext.setShowBlocked(false);
    friendsContext.setShowAddFriend(false);
  };

  const handleOnline = () => {
    setStateToFalse();
    friendsContext.setShowOnline(true);
  };
  
  const handleAll = () => {
    setStateToFalse();
    friendsContext.setShowAll(true);
  };

  const handlePending = () => {
    setStateToFalse();
    friendsContext.setShowPending(true);
  };

  const handleBlocked = () => {
    setStateToFalse();
    friendsContext.setShowBlocked(true);
  };

  const handleAddFriend = () => {
    setStateToFalse();
    friendsContext.setShowAddFriend(true);
  };

  const renderMain: () => JSX.Element | undefined = () => {
    /**
     * @private
     * @returns  {JSX.Element | undefined}  
     */
    if (friendsContext.showOnline) {
      return <AllFriends showOnlyOnline={ true } />
    } else if (friendsContext.showAll) {
      return <AllFriends />
    } else if (friendsContext.showPending) {
      return <Pendings />
    } else if (friendsContext.showBlocked) {
      return <BlockedFriends />
    } else if (friendsContext.showAddFriend) {
      return <AddFriend />
    }
  };

  return(
    <div className="friends">

      <header className="friends__header">
	<div className="friends__header__content">
          <h1 className="friends__header__title margin-right"><Users />Friends</h1>

          <button className={ "margin-right " + ((friendsContext.showOnline) ? "btn--active" : "btn") }
                  onClick={ handleOnline }>Online</button>
          <button className={ "margin-right " + ((friendsContext.showAll) ? "btn--active" : "btn") }
                  onClick={ handleAll }>All</button>
          <button className={ "margin-right " + ((friendsContext.showPending) ? "btn--active" : "btn") }
                  onClick={ handlePending }>Pending</button>
          <button className={ "margin-right " + ((friendsContext.showBlocked) ? "btn--active" : "btn") }
                  onClick={ handleBlocked }>Blocked</button>
          <button className="btn--success"
                  onClick={ handleAddFriend }>Add&nbsp;Friend</button>
	</div>

	<hr />
      </header>

      <main>
        { renderMain() }
      </main>

    </div>
  );
}

export default Friends;
