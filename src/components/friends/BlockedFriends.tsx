import React from "react";

import { useFriendsContext } from "../../contexts/FriendsContext";
import { FriendsContextValue } from "../../ts/types/contextvalue_types";
import { Friend } from "../../ts/types/friends_types"
import { TinyInt } from "../../ts/types/mariadb_types";

import BlockedItem from "./BlockedItem";

const BlockedFriends: React.FC = () => {

  const friendsContext: FriendsContextValue = useFriendsContext();

  const getBlockedFriends = () => {
    const allBlockedFriends: Array<Friend> = friendsContext.friends.filter(
      (friend: Friend) => friend.blocked === TinyInt.TRUE);
    return allBlockedFriends;
  };

  return(
    <ul className="userlist">
      { getBlockedFriends().map((friend: Friend) => (
        <BlockedItem
	  key={ friend.friendId }
          friendId={ friend.friendId }
	  friendName={ friend.friendName }
        />
      )) }
    </ul>
  );
}

export default BlockedFriends;
