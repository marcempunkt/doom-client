import React, { useState, ChangeEvent, FormEvent } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";

import { useAppContext } from "../../contexts/AppContext";
import type { AppContextValue } from "../../ts/types/contextvalue_types";
import type { FuzzyUser } from "../../ts/types/user_types";
import type { Response } from "../../ts/types/axios_types";

import Form from "../form/Form"; 
import Input from "../form/Input"; 
import SubmitButton from "../form/SubmitButton"; 
import FuzzyUsers from "./FuzzyUsers";

const AddFriend: React.FC = () => {

    const appContext: AppContextValue = useAppContext();

    const [query, setQuery] = useState<string>("");
    const [users, setUsers] = useState<Array<FuzzyUser>>([]);

    const handleQueryChange = (e: ChangeEvent<HTMLInputElement>) => setQuery(e.currentTarget.value);

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        /** Fuzzy search */
        e.preventDefault();
        axios.get(`${appContext.API_URL}/users/get/users/${query}`)
	     .then((res: AxiosResponse<Array<FuzzyUser>>) => {
	         setUsers(res.data);
	     })
	     .catch((err: AxiosError<Response>) => {
	         console.error(err);
	     });
    };

    return(
        <React.Fragment>
            <Form onSubmit={ handleSubmit } >
	        <Input
	            htmlId="friend"
	            label="Username or E-mail"
	            inputType="text"
	            value={ query }
	            onChange={ handleQueryChange } />

	        <SubmitButton
	            className="btn--primary full-width margin-top"
	            isDisabled={ (query) ? false : true }
	            innerHtml="Search" />

            </Form>

            <FuzzyUsers users={ users } />
        </React.Fragment>
    );
};

export default AddFriend;
