import React, { useState, ChangeEvent, FormEvent } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../../utils/logger";

import { useUserContext } from "../../contexts/UserContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useAppContext } from "../../contexts/AppContext";
import { AppContextValue,
	 UserContextValue,
	 NotificationContextValue,
	 ModalContextValue } from "../../ts/types/contextvalue_types";
import { NotificationType } from "../../ts/types/notification_types";
import { Response } from "../../ts/types/axios_types";

import Modal from "./Modal";
import Form from "./../form/Form";
import Input from "./../form/Input";
import SubmitButton from "./../form/SubmitButton";

const ChangePasswordModal: React.FC = () => {

    const userContext: UserContextValue = useUserContext();
    const notifContext: NotificationContextValue = useNotificationContext();
    const modalContext: ModalContextValue = useModalContext();
    const appContext: AppContextValue = useAppContext();

    const [oldPassword, setOldPassword] = useState<string>("");
    const [newPassword, setNewPassword] = useState<string>("");
    const [checkPassword, setCheckPassword] = useState<string>("");
    const [error, setError] = useState<string>("");

    const handleOldPwdChange = (e: ChangeEvent<HTMLInputElement>) => setOldPassword(e.currentTarget.value);
    const handleNewPwdChange = (e: ChangeEvent<HTMLInputElement>) => setNewPassword(e.currentTarget.value);
    const handleCheckPwdChange = (e: ChangeEvent<HTMLInputElement>) => setCheckPassword(e.currentTarget.value);

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!userContext.token) return logger.error.blue("ChangePasswordModal", "No Authentication Token found.");
        /* check if form is filled correctly */
        if (!oldPassword || !newPassword || !checkPassword) {
            return setError("Please fill out the form");
        } else if (newPassword !== checkPassword) {
            return setError("The new password is not identical!");
        } else if (oldPassword === newPassword) {
            return setError("Your old Password equals the new one! Choose another password.");
        } 

        /* Reset error message */
        setError("");

        axios.patch(
            `${appContext.API_URL}/users/change/password`,
            { oldPassword, newPassword },
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<Response>) => {
	    notifContext.create(NotificationType.SUCCESS, res.data.message);
	    modalContext.setShowChangePasswordModal(false);
	}).catch((err: AxiosError<Response>) => {
	    if (err.response) {
                setError(err.response.data.message);
	    }
	});
    };

    const handleCancelClick = () => modalContext.setShowChangePasswordModal(false);

    return(
        <Modal windowWidth="500px">
	    <Form
	        title="Change my password"
	        onSubmit={ handleSubmit }
	        error={ error } >

	        <Input
	            htmlId="oldPassword"
	            label="Current password"
	            inputType="password"
	            value={ oldPassword }
	            onChange={ handleOldPwdChange } />

	        <Input
	            htmlId="newPassword"
	            label="New password"
	            inputType="password"
	            value={ newPassword }
	            onChange={ handleNewPwdChange } />

	        <Input
	            htmlId="checkPassword"
	            label="Retype new password"
	            inputType="password"
	            value={ checkPassword }
	            onChange={ handleCheckPwdChange } />

                <button className="btn margin-top" onClick={ handleCancelClick } type="button">Cancel</button>

	        <SubmitButton
	            className="btn--primary"
	            isDisabled={ (oldPassword && newPassword && checkPassword) ? false : true }
	            innerHtml="Change password" />
	    </Form>
        </Modal>
    );
};

export default ChangePasswordModal;
