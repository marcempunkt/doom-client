import React, { useEffect, MouseEvent } from "react";
import "./Modal.scss";

import { useModalContext } from "../../contexts/ModalContext";
import { ModalContextValue } from "../../ts/types/contextvalue_types";

interface Props {
  windowWidth?: string;
  windowHeight?: string;
  children: JSX.Element | JSX.Element[];
}

const Modal: React.FC<Props> = (props: Props) => {

  const modalContext: ModalContextValue = useModalContext();

  const handleModalBackgroundClick: (_e: MouseEvent<HTMLDivElement>) => void = (_e) => modalContext.reset();

  const cancelPropagation: (e: MouseEvent<HTMLDivElement>) => void = (e) => e.stopPropagation();

  /* Global KeyListener for all Modals */
  useEffect(() => {
    /* IMPORTANT: 
     * In case of using this with React make sure you don't accidentally use import { KeyboardEvent } from "react";.
     * This leads to the not assignable to type 'EventListener' exception. (Credit: stackoverflow manu) 
     */
    const modalKeyHandler = (e: KeyboardEvent) => { if (e.key === "Escape") return modalContext.reset() };

    window.addEventListener("keydown", modalKeyHandler);

    return () => window.removeEventListener("keydown", modalKeyHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return(
    <div className={ "modal" } onClick={ handleModalBackgroundClick }>
      <div
	className={ "modal__window" }
	onClick={ cancelPropagation }
	style={ {
	  width: props.windowWidth,
	  height: props.windowHeight,
	} }
      >
        { props.children }
      </div>
    </div>
  );
};

Modal.defaultProps = {
  windowWidth: "500px"
};

export default Modal;
