import React from "react";

import { useModalContext } from "../../contexts/ModalContext";
import { ModalContextValue } from "../../ts/types/contextvalue_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalContent from "./ModalContent";
import CloseButton from "../../ui/CloseButton";

const BlogModal: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();

  const handleClose: () => void = () => {
    modalContext.setShowBlogModal(false);
  };

  return(
    <Modal windowWidth="95vw" windowHeight="95vh">
      <CloseButton onClick={ handleClose } />
      <ModalTitle>Babbeln News</ModalTitle>
      <ModalContent>
	<div className="selectable">
	  <p className="margin-top">HIER WIRD DER GANZE BLOG IM ACCORDION MENÜ LESBAR SEIN</p>
	</div>
      </ModalContent>
    </Modal>
  );
};

export default BlogModal;

