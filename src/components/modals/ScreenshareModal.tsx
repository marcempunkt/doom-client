import React, { useState, useEffect } from "react";
// import { ipcRenderer } from "electron";
import "./ScreenshareModal.scss";
import logger from "../../utils/logger";

import { useModalContext } from "../../contexts/ModalContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { useCallContext } from "../../contexts/CallContext";
import { ModalContextValue, NotificationContextValue, CallContextValue } from "../../ts/types/contextvalue_types";
import { ElectronAPI, PreviewSource } from "../../ts/types/electron_types";
import { NotificationType } from "../../ts/types/notification_types";

import Modal from "../modals/Modal";
import ModalTitle from "../modals/ModalTitle";
import ModalContent from "../modals/ModalContent";
import ModalButtonSection from "../modals/ModalButtonSection";
import ModalButton from "../modals/ModalButton";
import CloseButton from "../../ui/CloseButton";

const ScreenshareModal: React.FC = () => {
  /**
   * Screenshare Picker Modal for the electron version
   * @component
   */
  const modalContext: ModalContextValue = useModalContext();
  const notifContext: NotificationContextValue = useNotificationContext();
  const callContext: CallContextValue = useCallContext();

  const [previewSources, setPreviewSources] = useState<Array<PreviewSource>>([]);
  const [selected, setSelected] = useState<string>("");
  const [pressedLive, setPressedLive] = useState<boolean>(false);

  const handleCancelClick = () => {
    callContext.setShowScreen(false);
    modalContext.setShowScreenshareModal(false); 
  };

  const handleLiveClick = async () => {
    /**
     * Create mediastream from sourceId & append it to the peerConnection
     * @private
     */
    if (!selected) return logger.error.blue("ScreenshareModal", "Please select a window/screen for sharing.");
    if (!callContext.localStream) return logger.error.blue("ScreenshareModal", "No local stream.");
    if (!callContext.peerConnection) return logger.error.blue("ScreenshareMoal", "No peer connection.");

    setPressedLive(true);

    /*  navigator has to be casted as any because electron uses the old getUserMedia API */
    const stream: MediaStream | undefined = await (navigator as any).mediaDevices.getUserMedia({
      audio: false,
      video: {
	mandatory: {
	  chromeMediaSource: "desktop",
	  chromeMediaSourceId: selected, 
	},
      },
    }).catch((err: unknown) => {
      console.error(`${err}`);
      setPressedLive(false);
      return undefined;
    });

    if (!stream) return notifContext.create(NotificationType.ERROR, "Couldn't create stream.");

    callContext.startScreenshareSignaling(stream);
    modalContext.setShowScreenshareModal(false);
  };

  const handleGridViewItemClicked = (sourceId: string) => setSelected(sourceId);

  const shortenTitle: (title: string) => string = (title) => {
    // WWWWWWWWWWW...
    if (title.length < 15) return title; 
    return title.substring(0, 10) + "...";
  };

  useEffect(() => {
    /**
     * Get all the available streams
     * This fn should create a pop-up window that shows all available streams
     * the user picks one and then it should return the mediastream of that
     * particular stream
     * @effect
     */
    (async () => {
      const prevSources: Array<PreviewSource> =  await window.electron.ipcRenderer.invoke("get-screenshare-stream-previews");
      setPreviewSources(prevSources);
    })();
  }, []);

  return(
    <Modal>
      <CloseButton onClick={ handleCancelClick } />

      <ModalTitle>Select something to stream.</ModalTitle>

      <ModalContent>
	<div className="screensharemodal__gridview margin-top">
	  {
	    previewSources.map((source: PreviewSource) => {
	      return (
		<div
		  key={ source.id }
		  className="screensharemodal__gridview__item"
		  onClick={ () => handleGridViewItemClicked(source.id) }
		>
		  <div
		    className={ "screensharemodal__gridview__item__thumbnail" + ((source.id === selected) ? "--selected" : "") }
		  >
		    <img src={ source.thumbnail } />
		  </div>
		  <section className="screensharemodal__gridview__item__title">{ shortenTitle(source.name) }</section>
		</div>
	      );
	    })
	  }
	</div>
      </ModalContent>

      <ModalButtonSection>
	<ModalButton label="Cancel" onClick={ handleCancelClick } />
	<ModalButton className="btn--primary" label="Go live" onClick={ handleLiveClick } />
      </ModalButtonSection>
    </Modal>
  );
};

export default ScreenshareModal;
