import React, { useState, useEffect } from "react";

import { useMessageContext } from "../../contexts/MessageContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { MessageContextValue,
	 ModalContextValue,
	 FriendsContextValue, } from "../../ts/types/contextvalue_types";
import { Friend } from "../../ts/types/friends_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalButtonSection from "./ModalButtonSection";
import ModalButton from "./ModalButton";

const DeleteChatModal: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();
  const messageContext: MessageContextValue = useMessageContext();
  const friendsContext: FriendsContextValue = useFriendsContext();

  const [friend, setFriend] = useState<Friend | undefined>(undefined);

  useEffect(() => {
    /**
     * Get the friend item
     * @effect
     */
    const currentFriend: Friend | undefined = friendsContext.getFriend(modalContext.deleteChatFromUserId);
    setFriend(currentFriend);
  })

  const handleCancelClick = () => modalContext.setShowDeleteChatModal(false);

  const handleDeleteClick = () => {
    messageContext.deleteChat(modalContext.deleteChatFromUserId);
    modalContext.setShowDeleteChatModal(false);
  };

  useEffect(() => {
    /**
     * KeyListener
     * @effect
     */
    const windowKeyHandler = (event: KeyboardEvent) => {
      if (event.key === "Enter") return handleDeleteClick();
    };

    window.addEventListener("keydown", windowKeyHandler);

    return () => window.removeEventListener("keydown", windowKeyHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Modal>
      <ModalTitle>Delete the whole chat with { (friend) ? friend.friendName : "unknown" }?</ModalTitle>

      <ModalButtonSection>
	<ModalButton label="No" onClick={ handleCancelClick } />
	<ModalButton label="Delete everything!" className="btn--primary" onClick={ handleDeleteClick } />
      </ModalButtonSection>
    </Modal>
  );
};

export default DeleteChatModal;
