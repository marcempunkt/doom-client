import React, { useState, FormEvent } from "react";
import { useNavigate } from "react-router-dom";
import axios, { AxiosResponse, AxiosError } from "axios";
import "./Login.scss";

import { useAppContext } from "../contexts/AppContext";
import type { AppContextValue } from "../ts/types/contextvalue_types";
import type { Response } from "../ts/types/axios_types";

import Form from "./form/Form";
import Input from "./form/Input";
import SubmitButton from "./form/SubmitButton";

interface Props {
    onChangeView: () => void;
}

const Login: React.FC<Props> = (props: Props) => {
    /**
     * Component for rendering a Login Form
     * @component
     */
    const appContext: AppContextValue = useAppContext();
    
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [error, setError] = useState<string>("");
    const [disabled, setDisabled] = useState<boolean>(false);

    const navigate = useNavigate();

    const handleEmailChange = (e: FormEvent<HTMLInputElement>) => (setEmail(e.currentTarget.value));
    const handlePasswordChange = (e: FormEvent<HTMLInputElement>) => (setPassword(e.currentTarget.value));

    const clearAllState = () => {
        setEmail("");
        setPassword("");
        setError("");
    };

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        /**
         * Handles Login of a user
         * @private
         *
         * @param  {FormEvent<HTMLFormElement>}  e
         */
        e.preventDefault();

        if (!email || !password) return setError("Please fill out the form");

        setDisabled(true);

        axios.post(
            `${appContext.API_URL}/users/login`,
            { email, password },
        ).then((res: AxiosResponse<Response & { auth: string }>) => {
	    localStorage.setItem("auth", res.data.auth);
	    clearAllState();
	    navigate("/");
        }).catch((err: AxiosError<Response>) => {
	    setDisabled(false);
	    if (err.response) {
	        setError(err.response.data.message);
	    } else {
	        setError("Couldn't connect to the Server");
	    }
        });
    };

    return(
        <div className="start__form">
            <Form
	        title="Welcome Back"
	        onSubmit={ handleSubmit }
	        error={ error } >

	        <Input
	            htmlId="email"
	            label="E-Mail"
	            inputType="email"
	            value={ email }
	            onChange={ handleEmailChange } />

	        <Input
	            htmlId="password"
	            label="Password"
	            inputType="password"
	            value={ password }
	            onChange={ handlePasswordChange } />

	        <span className="start__form__changeview" onClick={ props.onChangeView }>{ "I don't have an account." }</span>

	        <SubmitButton
	            className="btn--primary full-width margin-top"
	            isDisabled={ disabled || ((email && password) ? false : true) }
	            innerHtml="Let me in!" />
            </Form>
        </div>
    );
};

export default Login;
