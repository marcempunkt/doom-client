import React, { ReactNode } from "react";
import "./ContextmenuItem.scss";

interface Props {
  icon: ReactNode;
  description: string;
  handleClick: () => void;
}

const ContextmenuItem: React.FC<Props> = (props: Props) => {

  /* Highlight some actions */
  const fontClass: string = (props.description === "Delete" || props.description === "Report") ?
			    " contextmenu__item--red" :
			    "";
  return(
    <div className={ "contextmenu__item" + fontClass } onClick={ props.handleClick }>
      { props.icon }
      <span>{ props.description }</span>
    </div>
  );
}

export default ContextmenuItem;
