import type { ServerMessage, SendMessage } from "./message_types";
import type { Friend, Friendrequest } from "./friends_types";
import type { Post, PostComment } from "./post_types";
import { Status } from "./user_types";

export enum SocketState {
  Disconnected = "disconnected",
  Connected = "connected",
}

export interface ServerToClientEvents {
  /* This type is used for socket.on */
  noArg: () => void;
  basicEmit: (a: number, b: string, c: Buffer) => void;
  withAck: (d: string, callback: (e: number) => void) => void;

  connected: (socketid: string) => void;
  /* messages */
  send_msg: (msg: ServerMessage) => void; 
  delete_msg: (deletedMessages: Array<ServerMessage>) => void;
  delete_chat: (deleteChatUserId: number) => void;
  /* friends */
  send_friendrequest: (newFriendrequest: Friendrequest) => void;
  decline_friendrequest: (pendingId: number) => void;
  accept_friendrequest: (pendingId: number, newFriend: Friend) => void;
  remove_friend: (friendId: number) => void;
  /* user */
  update_avatar: (friendId: number) => void;
  update_status: (friendId: number, status: Status) => void;
  update_username: (friendId: number, username: string) => void;
  client_update_avatar: () => void;
  client_update_status: (newStatus: Status) => void;
  client_update_username: (newUsername: string) => void;
  /* posts */
  send_post: (newPost: Post) => void;
  delete_post: (postId: number) => void;
  like_or_dislike_post: (userId: number, postId: number) => void;
  comment_post: (postId: number, newComment: PostComment) => void;
  remove_comment_post: (postId: number, commentId: number) => void;
  /* call / WebRTC */
  start_calling: (data: {
    fromUser: number; toUser: number, fromSocketid: string; key: string
  }) => void;
  end_calling: (data: {
    fromUser: number; toUser: number; toSocketid?: string
  }) => void;
  client_end_calling: (data: {
    userId: number; calling: number
  }) => void;
  accept_calling: (data: {
    fromUser: number; toUser: number; fromSocketid: string; toSocketid: string
  }) => void;
  signaling_icecandidate: (data: {
    fromUser: number; toUser: number; toSocketid: string; candidate: RTCIceCandidate | null; key: string
  }) => void;
  signaling_offer: (data: {
    fromUser: number; toUser: number; fromSocketid: string; toSocketid: string; offer: RTCSessionDescriptionInit; key: string
  }) => void;
  signaling_answer: (data: {
    fromUser: number; toUser: number; fromSocketid: string; toSocketid: string; answer: RTCSessionDescriptionInit; key: string
  }) => void;
  screenshare_offer: (data: { toSocketid: string; offer: RTCSessionDescriptionInit; key: string }) => void;
  screenshare_answer: (data: { toSocketid: string; answer: RTCSessionDescriptionInit; key: string }) => void;
  screenshare_icecandidate: (data: { toSocketid: string; candidate: RTCIceCandidate | null; key: string }) => void;
  screenshare_renegotiation_offer: (data: { toSocketid: string; offer: RTCSessionDescriptionInit; key: string }) => void;
  screenshare_renegotiation_answer: (data: { toSocketid: string; answer: RTCSessionDescriptionInit; key: string }) => void;
  screenshare_close: (data: { toSocketid: string; key: string }) => void;
  hangup: (data: {
    fromUser: number; toUser: number; toSocketid: string
  }) => void;
  renegotiation_offer: (data: {
    fromUser: number; toUser: number; toSocketid: string; offer: RTCSessionDescriptionInit; key: string
  }) => void;
  renegotiation_answer: (data: {
    fromUser: number; toUser: number; toSocketid: string; answer: RTCSessionDescriptionInit; key: string
  }) => void;
  calling_reconnected: (data: { key: string; fromUser: number; fromSocketid: string; inCallWith: number }) => void;
  calling_reconnected_answer: (data: { key: string; fromUser: number; fromSocketid: string; inCallWith: number; inCallWithSocketid: string }) => void;
}

export interface ClientToServerEvents {
  /* socket.emit() */
  /* messages */
  send_msg: (msg: SendMessage) => void; 
  delete_msg: (deletedMessages: Array<ServerMessage>) => void;
  delete_chat: (userId: number, deleteChatUserId: number) => void;
  /* friends */
  send_friendrequest: (fromUserid: number, toUserid: number) => void;
  decline_friendrequest: (pending: Friendrequest) => void;
  accept_friendrequest: (pendingId: number, fromUser: number, toUser: number) => void;
  remove_friend: (clientId: number, friendId: number) => void;
  /* user */
  update_avatar: (userId: number) => void;
  update_status: (userId: number, status: Status) => void;
  update_username: (userId: number, newUsername: string) => void;
  /* posts */
  send_post: (userId: number, newPost: Post) => void;
  delete_post: (userId: number, postId: number) => void;
  like_or_dislike_post: (userId: number, postId: number) => void;
  comment_post: (userId: number, postId: number, newComment: PostComment) => void;
  remove_comment_post: (userId: number, postId: number, commentId: number) => void;
  /* call / WebRTC */
  start_calling: (data: {
    fromUser: number; toUser: number, key: string
  }) => void;
  end_calling: (data: {
    fromUser: number; toUser: number; toSocketid?: string
  }) => void;
  client_end_calling: (data: {
    userId: number; calling: number
  }) => void;
  accept_calling: (data: {
    fromUser: number; toUser: number; toSocketid: string
  }) => void;
  signaling_icecandidate: (data: {
    fromUser: number; toUser: number; toSocketid: string; candidate: RTCIceCandidate | null; key: string
  }) => void;
  signaling_offer: (data: {
    fromUser: number; toUser: number; toSocketid: string; offer: RTCSessionDescriptionInit; key: string
  }) => void;
  signaling_answer: (data: {
    fromUser: number; toUser: number; fromSocketid: string; toSocketid: string; answer: RTCSessionDescriptionInit; key: string
  }) => void;
  screenshare_offer: (data: { toSocketid: string; offer: RTCSessionDescriptionInit; key: string }) => void;
  screenshare_answer: (data: { toSocketid: string; answer: RTCSessionDescriptionInit; key: string }) => void;
  screenshare_icecandidate: (data: { toSocketid: string; candidate: RTCIceCandidate | null; key: string }) => void;
  screenshare_renegotiation_offer: (data: { toSocketid: string; offer: RTCSessionDescriptionInit; key: string }) => void;
  screenshare_renegotiation_answer: (data: { toSocketid: string; answer: RTCSessionDescriptionInit; key: string }) => void;
  screenshare_close: (data: { toSocketid: string; key: string }) => void;
  hangup: (data: {
    fromUser: number; toUser: number; toSocketid: string
  }) => void;
  renegotiation_offer: (data: {
    fromUser: number; toUser: number; toSocketid: string; offer: RTCSessionDescriptionInit; key: string
  }) => void;
  renegotiation_answer: (data: {
    fromUser: number; toUser: number; toSocketid: string; answer: RTCSessionDescriptionInit; key: string
  }) => void;
  calling_reconnected: (data: { key: string; fromUser: number; fromSocketid: string; inCallWith: number }) => void;
  calling_reconnected_answer: (data: { key: string; fromUser: number; fromSocketid: string; inCallWith: number; inCallWithSocketid: string }) => void;
}
