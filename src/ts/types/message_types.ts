import { TinyInt } from "./mariadb_types";

export type MessageState = Array<ClientMessage>;

export interface Draft {
  userId: number;
  content: string;
}

export enum MessageType {
  SEND_MSG = "send-msg",
  REC_MSG = "rec-msg",
}

/* Message Type that is stored inside the Client's State*/
export interface ClientMessage extends ServerMessage {
  is_deleted: TinyInt;
  // TODO is_submitting: boolean | TinyInt
}

/* Message Type that comes from the backend-server */
export interface ServerMessage {
  id: number;
  owner: number;
  from_user: number;
  to_user: number;
  content: string;
  send_at: number;
}

/* Message type for sending a message
 * all necessary fields the back-end needs */
export interface SendMessage {
  from_user: number;
  to_user: number;
  content: string;
}

export enum ReducerActionType {
  FETCH,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  DELETE_CHAT,
  LOGOUT,
}

export interface ReducerAction {
  type: ReducerActionType;
  payload?: any;
}

