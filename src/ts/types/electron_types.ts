export type InputSource = any;

export interface PreviewSource {
  id: string;
  name: string;
  thumbnail: string;
}

export interface ElectronAPI {
  ipcRenderer: any;
  shell: {
    openExternal: (url: string) => void;
  }
}

declare global {
  interface Window {
    electron: ElectronAPI,
  }
}
