import { TinyInt } from "./mariadb_types";

export enum Status {
    ON = "on",
    OFF = "off",
    DND = "dnd",
    AWAY = "away",
}

export interface DbFriend {
    id: number;
    user_id: number;
    friend_with: number;
    blocked: TinyInt;
}

// TODO
export interface Friend {
    friendId: number;  // user_id from user
    friendName: string; // username from user
    status: Status; // status from user
    registerDate: number; // register_date from user
    blocked: TinyInt; // blocked from friend
}

export interface FriendWithAvatar extends Friend {
    avatar: string;
}

export interface Friendrequest {
    id: number;
    from_user: number;
    to_user: number;
}

export enum FriendsReducerActionType {
    FETCH,
    REMOVE,
    ADD,
    BLOCK,
    UNBLOCK,
    UPDATE_USERNAME,
    UPDATE_STATUS,
    UPDATE_AVATAR,
    LOGOUT,
}

export interface FriendsReducerAction {
    type: FriendsReducerActionType;
    payload?: any;
}

export enum PendingsReducerActionType {
    FETCH,
    REMOVE,
    ADD,
    LOGOUT,
}

export interface PendingsReducerAction {
    type: PendingsReducerActionType;
    payload?: any;
}
