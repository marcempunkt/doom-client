import React, { useState, useEffect, MouseEvent } from "react";

import { useFriendsContext } from "../contexts/FriendsContext";
import { useUserContext } from "../contexts/UserContext";
import { FriendsContextValue, UserContextValue } from "../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../ts/types/friends_types";
import { TinyInt } from "../ts/types/mariadb_types";

interface Props {
    userId: number;
    onContextmenu: (e: MouseEvent<HTMLLIElement>) => void;
    className?: string;
    onClick?: () => void;
    showActions?: boolean;
    actions?: Array<JSX.Element>;
}

const GenericUserlistItem: React.FC<Props> = (props: Props) => {

    const friendsContext: FriendsContextValue = useFriendsContext();
    const userContext: UserContextValue = useUserContext();

    /* Get the correct friend(-item) of the user */
    const [friend, setFriend] = useState<FriendWithAvatar | undefined>(undefined);

    useEffect(() => {
        /**
         * Get the correct Friend Object according to the passed down userId
         * @effect
         */
        const currentFriend: FriendWithAvatar | undefined = friendsContext.getFriend(props.userId);
        if (currentFriend) return setFriend(currentFriend);
    }, [props.userId, friendsContext.friends]);

    const handleName: () => string = () => {
        /**
         * shorten the username if too long
         * @private
         */
        if (!friend) return "unknown";

        if (friend.friendName.length > 10) {
            return (friend.friendName.slice(0, 9) + "...");
        }

        return friend.friendName;
    };

    const getProfilePicClassNames: () => string = () => {
        /**
         * returns additional className for the user picture 
         * depending if blocked
         * @private
         * 
         * returns  {string}  classes
         */
        let classes: string = "";

        if (!friend) return classes += " status--offline";

        classes += userContext.handleStatusClasses(friend.status);

        /* Check if blocked */
        if (friend.blocked === TinyInt.TRUE) {
            classes += " useritem__icon--blocked";
        }

        return classes;
    };

    return(
        <li className={ "useritem"
                     + ((props.showActions) ? " useritem--always-show-actions " : "")
                     + ((props.className) ? ` ${props.className}` : "") }
	    onContextMenu={ props.onContextmenu }
	    onClick={ props.onClick } >

            <img
                className={ "useritem__icon" + getProfilePicClassNames() }
                alt={ (friend) ? friend.friendName : "unknown" }
                src={ friendsContext.getAvatar(friend) } />

            <span className="useritem__name">{ handleName() }</span>

            { (props.actions) ?
              <div className="useritem__actions">
	          { props.actions.map((action: JSX.Element) => action) }
              </div>
              : null }

        </li>
    );
};

export default GenericUserlistItem;
